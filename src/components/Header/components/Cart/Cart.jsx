import React,{Component} from "react";
import '../Cart/Cart.scss'
class Cart extends Component{

    
    render(){
        const {cartCount} = this.props
        return(
            
                <div className="cart">
                    <img className="cart__icon" src="/img/cart.png" alt="cart" />
                    <p className="cart__text">Cart</p>
                    <p className="cart__count">({cartCount})</p>
                </div>
            
            
        )
    }
}
export default Cart;