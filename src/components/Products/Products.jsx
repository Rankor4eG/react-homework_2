import React,{Component} from "react";
import '../Products/Products.scss'
import Card from "./components/Card/Card";

class Products extends Component{
state = {
    products: []
}
    componentDidMount(){
        
        fetch('/products.json')
        .then(response => response.json())
        .then(products =>{

            this.setState({
                products: products,
            })
            
        })
    }
    render(){
            const {products} = this.state
            const product = products.map(card =>{
                 const isCardInCart = this.props.cart.includes(card.id)
                 const isCardInFavourite = this.props.favourite.includes(card.id)
                return <Card 
                            addToCart={this.props.addToCart}
                            removeFromCart = {this.props.removeFromCart}  
                            cart={this.props.cart} 
                            product ={card}
                            isCardInCart = {isCardInCart}
                            favourite= {this.props.favourite}
                            addToFavourite = {this.props.addToFavourite}
                            removeFromFavourite = {this.props.removeFromFavourite}
                            isCardInFavourite = {isCardInFavourite}
                            />
            })
            
        return(
            <div className="products">
                <div className="products__container">
                    <h2 className="products__title">CARS CURRENTLY ON SALE</h2>
                    <div className="products__wrap">
                        
                        {product}
                    </div>
                    
                </div>
                
                
            </div>
        )
    }
}
export default Products;