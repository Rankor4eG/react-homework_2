import React,{Component} from "react";
import '../Card/Card.scss'
import Button from "../../../Button";
import Modal from "../../../Modal";

class Card extends Component{

    state={
        modalCart: false,
        fav: false,
    }


    showCartModal =()=>{
        this.setState({
            modalCart: true,
        })
    }
    closeCartModal =()=>{
        this.setState({
            modalCart: false,
        })
    }
   
    
    addToFavourite = () => {
        if (this.props.isCardInFavourite) {
          this.props.removeFromFavourite(this.props.product.id)
        } else {
          this.props.addToFavourite(this.props.product.id)
        }
      }
    addToCart = () => {
        if(this.props.isCardInCart){
            this.props.removeFromCart(this.props.product.id)
        }else{
            this.props.addToCart(this.props.product.id)
        }
        
      }
    render(){
        
        const {url,title,color,price } = this.props.product
        
        
        const actionsModalCart =
        
        <div className="modal__footer-actions">
          <Button text='add' onClick={this.addToCart} className='button__modal-cart--footer'/>
          <Button text='cancel'onClick={this.closeCartModal} className='button__modal-cart--footer'/> 
          </div>
        
        return(
            <div className="card">
                <div className="card__header">
                    <img className="card__header-img" src={url} alt="img" />
                    <div className="favourite__icon-wrapper" onClick={this.addToFavourite}>
                        
                        <img className="favourite__icon" 
                            src={this.props.isCardInFavourite ?'/img/star-active.png' :'/img/star.png'  } 
                             alt="star" 
                            />
                        
                    </div>
                    
                </div>
                <div className="card__content">
                    <p className="card__content-title">{title}</p>
                    <p className="card__content-color">Color: {color}</p>
                </div>
                <div className="card__footer">
                    <p className="card__footer-price">{price}</p>
                    <Button className='card__footer-button' onClick={this.showCartModal}  text = 'add to cart'/>
                    {this.state.modalCart &&
                        <Modal
                            header='Do you want to add this product to the cart?'
                            text='This product will be added to the cart!'
                            btnClose={this.closeCartModal}
                            actions={actionsModalCart}
                        />
                    
                    }
                </div>
            </div>
        )
    }
}
export default Card;